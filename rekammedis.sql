-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 28, 2021 at 05:54 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rekammedis`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `ID_ADMIN` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `NAMA_ADMIN` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(40) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `STATUS` int(5) DEFAULT NULL,
  `JOB_DESC` varchar(20) DEFAULT NULL,
  `pic` varchar(255) NOT NULL DEFAULT '/img/staff.jpg',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daftar_periksa`
--

CREATE TABLE `daftar_periksa` (
  `ID_DAFTAR` int(11) NOT NULL,
  `ID_PASIEN` int(11) DEFAULT NULL,
  `ID_DOKTER` int(11) DEFAULT NULL,
  `TANGGAL_PERIKAS` date DEFAULT NULL,
  `KELUHAN` text DEFAULT NULL,
  `STATUS` tinyint(5) DEFAULT 0,
  `WAKTU_DAFTAR` time DEFAULT NULL,
  `ID_URUT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `ID_DOKTER` int(11) NOT NULL,
  `ID_USER` int(11) DEFAULT 1,
  `NAMA_DOKTER` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(20) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `NO_PRAKTEK` varchar(20) DEFAULT NULL,
  `SPESIALIS` varchar(30) DEFAULT NULL,
  `STATUS` int(5) DEFAULT 0,
  `STATUS_AKUN` int(11) NOT NULL DEFAULT 10
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_dokter`
--

CREATE TABLE `jadwal_dokter` (
  `ID_JADWAL` int(11) NOT NULL,
  `WAKTU_MULAI` time DEFAULT NULL,
  `WAKTU_SELESAI` time DEFAULT NULL,
  `ID_DOKTER` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1511447175),
('m130524_201442_init', 1511447178);

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE `obat` (
  `ID_OBAT` int(11) NOT NULL,
  `NAMA_OBAT` varchar(40) DEFAULT NULL,
  `HARGA` int(11) DEFAULT NULL,
  `PRODUKSI` varchar(50) NOT NULL,
  `JUMLAH_STOK` int(10) NOT NULL,
  `TANGGAL_MASUK` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `ID_PASIEN` int(11) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `NAMA_PASIEN` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(40) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `BERAT_BADAN` int(11) DEFAULT NULL,
  `TINGGI_BADAN` int(11) DEFAULT NULL,
  `GOL_DARAH` varchar(2) DEFAULT NULL,
  `STATUS` int(5) DEFAULT 10
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `ID_PEMBAYARAN` int(11) NOT NULL,
  `BIAYA_PERIKSA` varchar(20) DEFAULT NULL,
  `TOTAL` varchar(20) DEFAULT NULL,
  `ID_RESEP` int(11) DEFAULT NULL,
  `ID_PERIKSA` int(11) NOT NULL,
  `BAYAR` varchar(20) NOT NULL,
  `KEMBALI` varchar(20) NOT NULL,
  `STATUS` tinyint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `periksa`
--

CREATE TABLE `periksa` (
  `ID_PERIKSA` int(11) NOT NULL,
  `DIAGNOSA` text DEFAULT NULL,
  `CATATAN` text DEFAULT NULL,
  `ID_DOKTER` int(11) DEFAULT NULL,
  `ID_PASIEN` int(11) DEFAULT NULL,
  `ID_DAFTAR` int(11) NOT NULL,
  `STATUS` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `resep`
--

CREATE TABLE `resep` (
  `ID_RESEP` int(11) NOT NULL,
  `TOTAL_HARGA` varchar(20) DEFAULT NULL,
  `ID_OBAT` int(11) DEFAULT NULL,
  `ID_PERIKSA` int(11) DEFAULT NULL,
  `DOSIS` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `ID_STAFF` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `NAMA_STAFF` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(20) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `JOB_DESC` varchar(20) NOT NULL,
  `STATUS` int(5) DEFAULT 10,
  `pic` varchar(255) NOT NULL DEFAULT '/img/staff.jpg',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id_pasien` varchar(10) NOT NULL,
  `token` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`id_pasien`, `token`) VALUES
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('7', 'null'),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('10', 'eXe9Yasj2HI:APA91bH1-8g9Zn9MovoPY57iLYVsNeRZfZXmrp9svl0jqLo8W0KlJHH8gtT08eFZEqWwRAt9RBAI1t1Fwask11cr2kNvFuNUeWn7zs0xT5SBD0cZQimuLbJ6O-n9bhmfaXBII0J3Yhzu'),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('', ''),
('5', 'null'),
('8', 'eXe9Yasj2HI:APA91bH1-8g9Zn9MovoPY57iLYVsNeRZfZXmrp9svl0jqLo8W0KlJHH8gtT08eFZEqWwRAt9RBAI1t1Fwask11cr2kNvFuNUeWn7zs0xT5SBD0cZQimuLbJ6O-n9bhmfaXBII0J3Yhzu'),
('', ''),
('', '');

-- --------------------------------------------------------

--
-- Table structure for table `urut_periksa`
--

CREATE TABLE `urut_periksa` (
  `ID_URUT` int(11) NOT NULL,
  `NO_URUT` int(11) DEFAULT NULL,
  `WAKTU_PERIKSA` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `urut_periksa`
--

INSERT INTO `urut_periksa` (`ID_URUT`, `NO_URUT`, `WAKTU_PERIKSA`) VALUES
(1, 1, '08:00:00'),
(2, 2, '08:15:00'),
(3, 3, '08:30:00'),
(4, 4, '08:45:00'),
(5, 5, '09:00:00'),
(6, 6, '09:15:00'),
(7, 7, '09:30:00'),
(8, 8, '09:45:00'),
(9, 9, '10:00:00'),
(10, 10, '10:15:00'),
(11, 11, '10:30:00'),
(12, 12, '10:45:00'),
(13, 13, '11:00:00'),
(14, 14, '11:15:00'),
(15, 15, '11:30:00'),
(16, 16, '11:45:00'),
(17, 17, '12:00:00'),
(18, 18, '12:15:00'),
(19, 19, '12:30:00'),
(20, 20, '12:45:00'),
(21, 21, '13:00:00'),
(22, 22, '13:15:00'),
(23, 23, '13:30:00'),
(24, 24, '13:45:00'),
(25, 25, '14:00:00'),
(26, 26, '14:15:00'),
(27, 27, '14:30:00'),
(28, 28, '14:45:00'),
(29, 29, '15:00:00'),
(30, 30, '15:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `role` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `role`, `created_at`, `updated_at`) VALUES
(6, 'adminadmin', 'dS2UsOUgA0JkxAZocoJ3z0yuRW9YKeqC', '$2y$13$5rBfs2Q3LhzdVyOwCU19K.RghTuSXsQiaBnoTPtiUvJSS9rgTsdf2', NULL, 'adminadmin@gmail.com', 10, 20, '2021-05-28 10:51:46', '2021-05-28 10:51:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ID_ADMIN`),
  ADD KEY `ID_USER` (`user_id`);

--
-- Indexes for table `daftar_periksa`
--
ALTER TABLE `daftar_periksa`
  ADD PRIMARY KEY (`ID_DAFTAR`),
  ADD KEY `ID_PASIEN` (`ID_PASIEN`),
  ADD KEY `ID_DOKTER` (`ID_DOKTER`),
  ADD KEY `ID_URUT` (`ID_URUT`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`ID_DOKTER`),
  ADD KEY `ID_USER` (`ID_USER`);

--
-- Indexes for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  ADD PRIMARY KEY (`ID_JADWAL`),
  ADD KEY `ID_DOKTER` (`ID_DOKTER`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`ID_OBAT`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`ID_PASIEN`),
  ADD KEY `ID_USER` (`ID_USER`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`ID_PEMBAYARAN`),
  ADD KEY `ID_RESEP` (`ID_RESEP`),
  ADD KEY `ID_PERIKSA` (`ID_PERIKSA`);

--
-- Indexes for table `periksa`
--
ALTER TABLE `periksa`
  ADD PRIMARY KEY (`ID_PERIKSA`),
  ADD KEY `ID_DOKTER` (`ID_DOKTER`),
  ADD KEY `ID_PASIEN` (`ID_PASIEN`),
  ADD KEY `ID_DAFTAR` (`ID_DAFTAR`);

--
-- Indexes for table `resep`
--
ALTER TABLE `resep`
  ADD PRIMARY KEY (`ID_RESEP`),
  ADD KEY `ID_OBAT` (`ID_OBAT`),
  ADD KEY `ID_PERIKSA` (`ID_PERIKSA`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`ID_STAFF`),
  ADD KEY `ID_USER` (`user_id`);

--
-- Indexes for table `urut_periksa`
--
ALTER TABLE `urut_periksa`
  ADD PRIMARY KEY (`ID_URUT`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `ID_ADMIN` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daftar_periksa`
--
ALTER TABLE `daftar_periksa`
  MODIFY `ID_DAFTAR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `dokter`
--
ALTER TABLE `dokter`
  MODIFY `ID_DOKTER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  MODIFY `ID_JADWAL` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `obat`
--
ALTER TABLE `obat`
  MODIFY `ID_OBAT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `ID_PASIEN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `ID_PEMBAYARAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `periksa`
--
ALTER TABLE `periksa`
  MODIFY `ID_PERIKSA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `resep`
--
ALTER TABLE `resep`
  MODIFY `ID_RESEP` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `ID_STAFF` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `urut_periksa`
--
ALTER TABLE `urut_periksa`
  MODIFY `ID_URUT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `daftar_periksa`
--
ALTER TABLE `daftar_periksa`
  ADD CONSTRAINT `daftar_periksa_ibfk_1` FOREIGN KEY (`ID_PASIEN`) REFERENCES `pasien` (`ID_PASIEN`),
  ADD CONSTRAINT `daftar_periksa_ibfk_2` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`),
  ADD CONSTRAINT `daftar_periksa_ibfk_3` FOREIGN KEY (`ID_URUT`) REFERENCES `urut_periksa` (`ID_URUT`);

--
-- Constraints for table `dokter`
--
ALTER TABLE `dokter`
  ADD CONSTRAINT `dokter_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  ADD CONSTRAINT `jadwal_dokter_ibfk_1` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`);

--
-- Constraints for table `pasien`
--
ALTER TABLE `pasien`
  ADD CONSTRAINT `pasien_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `periksa`
--
ALTER TABLE `periksa`
  ADD CONSTRAINT `periksa_ibfk_1` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`),
  ADD CONSTRAINT `periksa_ibfk_2` FOREIGN KEY (`ID_PASIEN`) REFERENCES `pasien` (`ID_PASIEN`),
  ADD CONSTRAINT `periksa_ibfk_3` FOREIGN KEY (`ID_DAFTAR`) REFERENCES `daftar_periksa` (`ID_DAFTAR`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
