###  REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install the application using the following command:

~~~
Clone Repo
composer self-update
composer global require "fxp/composer-asset-plugin:~1.1.1"
cd Rekam Medis-web
composer install
~~~

### Install from an Archive File
Extract the archive file downloaded from this repository to
a directory named Rekam Medis-web that is directly under the Web root.

CONFIGURATION
-------------
After you install the application, you have to conduct the following steps to initialize
the installed application. You only need to do these once for all.

1. Run command `init` to initialize the application with a specific environment.
2. Create a new database and adjust the `components['db']` configuration in `common/config/main-local.php` accordingly.

Edit the file `common/config/main-local.php` with real data, for example:

```php
'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=rekammedis',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `common/config/` directory to customize your application as required.

3. Apply migrations with console command `yii migrate`. This will create tables needed for the application to work. Or can import a database that has been provided
4. Set document roots of your Web server:

- for frontend `/path/to/Rekam Medis-web/frontend/web/`
- for backend `/path/to/Rekam Medis-web/backend/web/`

To login into the application, you need to first sign up, with any of your email address, username and password.
Then, you can login into the application with same email address and password at any time.

### Login User
Username : adminadmin
Password : password